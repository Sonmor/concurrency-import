using NUnit.Framework;
using Moq;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using System.Collections.Generic;
using System;
using Otus.Teaching.Concurrency.Import.Core.IO;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Tests
{
    [TestFixture]
    public class GeneratorFactoryTests
    {
        GeneratorFactory Create(IGeneratorApp app = null)
        {
            app ??= new Mock<IGeneratorApp>().Object;
            return new GeneratorFactory(app);
        }
        public static IEnumerable<Tuple<string, IDataGenerator>> CorrectData = new List<Tuple<string, IDataGenerator>>
        {
            new Tuple<string, IDataGenerator>("blablabla.csv", new CsvSerialyzer(null)),
            new Tuple<string, IDataGenerator>("blablabla.xml", new XmlSerialyzer(null))
        };
        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);
        [TestCaseSource(nameof(CorrectData))]
        public void GetGenerator(Tuple<string, IDataGenerator> data)
        {
            var mock = new Mock<IGeneratorApp>();
            mock.Setup(x => x.OutputFile).Returns(data.Item1);
            var factory = Create(mock.Object);
            Assert.That(factory.GetGenerator().GetType(), Is.EqualTo(data.Item2.GetType()));
        }
    }
}