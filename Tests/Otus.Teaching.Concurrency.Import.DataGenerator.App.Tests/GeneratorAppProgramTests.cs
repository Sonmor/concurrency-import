using NUnit.Framework;
using Moq;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.IO;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.App.Tests
{
    public class GeneratorAppProgramTests
    {
        private GeneratorAppProgram Create(IGeneratorApp app = null)
        {
            app ??= new Mock<IGeneratorApp>().Object;
            return new GeneratorAppProgram(app);
        }
        public static IEnumerable<string[]> WrongCommandLineArgs = new List<string[]>
        {
            null,
            System.Array.Empty<string>(),
            new string[] {""}
        };
        public static IEnumerable<string[]> CorrectCommandLineArgs = new List<string[]>
        {
            @"set -o c:\temp\customers.xml -c 1000".Split(' '),
            @"set -c 1000 -o c:\temp\customers.xml".Split(' '),
            @"set --output c:\temp\customers.xml --count 2000".Split(' ')
        };
        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);

        [TestCaseSource(nameof(WrongCommandLineArgs))]
        public void TryValidateAndParseArgs_IsNullOrEmpty_ApplicationWriteError(string[] args)
        {
            var mock = new Mock<IGeneratorApp>();
            var prog = Create(mock.Object);
            _ = prog.TryValidateAndParseArgs(args);
            mock.Verify(x => x.OnThrowError(It.IsAny<string>()), Times.Once);
        }
        [TestCaseSource(nameof(WrongCommandLineArgs))]
        public void TryValidateAndParseArgs_IsNullOrEmpty_ReturnFalse(string[] args)
        {
            var mock = new Mock<IGeneratorApp>();
            var prog = Create(mock.Object);
            var result = prog.TryValidateAndParseArgs(args);
            Assert.That(result, Is.False);
        }
        [TestCaseSource(nameof(CorrectCommandLineArgs))]
        public void TryValidateAndParseArgs_CorrectCommandLineArgs_ReturnedTrue(string[] args)
        {
            var mock = new Mock<IGeneratorApp>();
            var prog = Create(mock.Object);
            var result = prog.TryValidateAndParseArgs(args);
            Assert.That(result, Is.True);
        }

        [TestCaseSource(nameof(CorrectCommandLineArgs))]
        public void Application_CorrectCommandLineArgs_ExpectedOutputFileValues(string[] args)
        {
            var mock = new Mock<IGeneratorApp>();
            var expected = @"c:\temp\customers.xml";
            mock.SetupSet(x => x.OutputFile = expected);
            var prog = Create(mock.Object);
            prog.ParseSettings(args);
            mock.VerifySet(x => x.OutputFile = expected);
        }
    }
}