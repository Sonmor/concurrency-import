using NUnit.Framework;
using Moq;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.IO;
using System;

namespace Otus.Teaching.Concurrency.Import.Loader.Tests
{
    [TestFixture]
    public class LoaderAppProgrammTests
    {
        private LoaderAppProgram Create(ILoaderApp app = null) => new LoaderAppProgram(app ?? new Mock<ILoaderApp>().Object);
       
        public static IEnumerable<Tuple<string, string, int, string[]>> CorrectCommandLineArgs
            => new List<Tuple<string, string, int,string[]>>
            {
                new Tuple<string, string, int, string[]>(@"c:\temp\generator.exe", @"c:\temp\customers.csv",1000,@"set -g c:\temp\generator.exe -o c:\temp\customers.csv -c 1000".Split(' ')),
                new Tuple<string, string, int,string[]> (@"c:\temp\generator.exe", @"c:\temp\customers.csv", 100, @"set -g c:\temp\generator.exe -o c:\temp\customers.csv".Split(' ')),
                new Tuple<string, string, int, string[]>(@"c:\temp\generator.exe", @"c:\temp\customers.csv", 100, @"set --generator c:\temp\generator.exe -o c:\temp\customers.csv".Split(' ')),
                new Tuple<string, string, int, string[]>(null, @"c:\temp\customers.csv", 1000, @"set -o c:\temp\customers.csv -c 1000".Split(' '))
            };

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);
        [TestCaseSource(nameof(CorrectCommandLineArgs))]
        public void ParseSettings_SettingsAreExpected_True(Tuple<string, string, int, string[]> items)
        {
            var app = new Mock<ILoaderApp>();
            
            var prog = Create(app.Object);
            prog.ParseSettings(items.Item4);

            app.VerifySet(x => x.GeneratorPath = items.Item1);
            app.VerifySet(x => x.OutputFile = items.Item2);
            app.VerifySet(x => x.DataCount = items.Item3);
        }
    }
}