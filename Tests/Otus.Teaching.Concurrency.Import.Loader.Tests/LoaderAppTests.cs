﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.IO;
using NUnit.Framework;
using Moq;
namespace Otus.Teaching.Concurrency.Import.Loader.Tests
{
    [TestFixture]
    public class LoaderAppTests
    {
        private ILoaderApp Create(IWriterUI writer = null, IReaderUI reader = null, IErrorUI error = null)
        {
            writer ??= new Mock<IWriterUI>().Object;
            reader ??= new Mock<IReaderUI>().Object;
            error ??= new Mock<IErrorUI>().Object;
            return new LoaderApp(writer, reader, error) ;
        }
        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);
        [Test]
        public void OnWriter_IsCallWriterUIWriter_True()
        {
            var mock = new Mock<IWriterUI>();
            var app = Create(writer: mock.Object);
            app.OnWrite("Test message");
            mock.Verify(x => x.Write("Test message"), Times.Once);
        }
        [Test]
        public void OnThrowError_IsCallErrorUIThrowError_True()
        {
            var mock = new Mock<IErrorUI>();
            var app = Create(error: mock.Object);
            app.OnThrowError("Test error");
            mock.Verify(x => x.ThrowError("Test error"), Times.Once);
        }
        [Test]
        public void OnRead_IsCallReaderUIRead_True()
        {
            var mock = new Mock<IReaderUI>();
            mock.Setup(x => x.Read()).Returns("test input");
            var app = Create(reader: mock.Object);
            var actual = app.OnRead();
            Assert.That(actual, Is.EqualTo("test input"));
            mock.Verify(x => x.Read(), Times.Once);
        }
    }
}
