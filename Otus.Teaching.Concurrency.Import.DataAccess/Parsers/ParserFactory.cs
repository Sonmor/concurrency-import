﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public static class ParserFactory
    {
        public static ParserBase GetParser(IGeneratorApp app)
        {
            if (!File.Exists(app.OutputFile))
            {
                app.OnThrowError($"The {app.OutputFile} is not exist\n");
                app.OnExit();
            }
            switch (Path.GetExtension(app.OutputFile))
            {
                case ".csv":
                    return new CsvParser(app);
                case ".xml":
                    return new XmlParser(app);
                default:
                    app.OnThrowError($"Incorrect data format");
                    app.OnExit();
                    return null;
            }
        }
    }
}
