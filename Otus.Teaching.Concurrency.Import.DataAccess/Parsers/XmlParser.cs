﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.IO;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : ParserBase
    {
        public XmlParser(IGeneratorApp app) : base(app) { }
        public override IEnumerable<Customer> GetItems(FileStream stream)
        {
            var serialyzer = new XmlSerializer(typeof(CustomersList));
            return ((CustomersList)serialyzer.Deserialize(stream)).Customers;
        }
    }
}