﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using System.IO;
using CsvHelper;
using System.Globalization;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System;
using CsvHelper.Configuration;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : ParserBase
    {
        public CsvParser(IGeneratorApp app) : base(app) { }
        public override IEnumerable<Customer> GetItems(FileStream stream)
        {
            var config = new CsvConfiguration(CultureInfo.CurrentCulture)
            {
                NewLine = Environment.NewLine,
                Delimiter = "," 
            };
            var reader = new CsvReader(new StreamReader(stream), config);
            return reader.GetRecords<Customer>();
        }
    }
}
