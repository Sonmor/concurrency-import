﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public abstract class ParserBase : IDataParser<IEnumerable<Customer>>
    {
        private readonly IGeneratorApp app;
        protected ParserBase(IGeneratorApp app)
        {
            this.app = app;
        }
        public abstract IEnumerable<Customer> GetItems(FileStream stream);
        public IEnumerable<Customer> Parse()
        {
            try
            {
                using (FileStream stream = new FileStream(app.OutputFile, FileMode.Open))
                {
                    return GetItems(stream).ToList();
                }
            }
            catch(Exception ex)
            {
                app.OnThrowError(ex.Message);
                app.OnExit();
                return Enumerable.Empty<Customer>();
            }
        }
    }
}
