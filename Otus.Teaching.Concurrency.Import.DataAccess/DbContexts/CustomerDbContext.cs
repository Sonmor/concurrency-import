﻿using System.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Sqlite;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.DbContexts
{
    public class CustomerDbContext:DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // var con = ConfigurationManager.ConnectionStrings["Sqlite"].ConnectionString;
            
            optionsBuilder.UseSqlite(@"Data Source=.\customers.db");
            base.OnConfiguring(optionsBuilder);
        }
            
    }
}
