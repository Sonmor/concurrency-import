using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.DataAccess.DbContexts;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        public async Task AddCustomerAsync(Customer customer)
        {
            using (var context = new CustomerDbContext())
            {
                await context.AddAsync(customer);
                await context.SaveChangesAsync();
            }
        }

        public async Task AddCustomersAsync(IEnumerable<Customer> customers)
        {
            using (var context = new CustomerDbContext())
            {
                await context.Customers.AddRangeAsync(customers);
                await context.SaveChangesAsync();
            }
        }

        public async Task<Customer> FindCustomerAsync(int id)
        {
            using (var context = new CustomerDbContext())
            {
                return await context.Customers.FindAsync(id);
            }
        }

        public async Task<List<Customer>> GetAllCustomersAsync()
        {
            using (var context = new CustomerDbContext())
            {
                return await context.Customers.AsNoTracking().ToListAsync();
            }
        }
    }
}