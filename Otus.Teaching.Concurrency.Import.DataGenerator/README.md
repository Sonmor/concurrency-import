 # Otus.Teaching.Concurrency.Import.DataGenerator

   Библиотека, в которой должна определена логика генерации файла с данными, в базовом варианте это XML.

## Структура проекта

- **GeneratorFactory** - в зависимости от расширения файла создает соответствующий сериализатор

```c#
public IDataGenerator GetGenerator()
{
    IDataGenerator result;
    var ext = Path.GetExtension(app.OutputFile);
    switch (ext)
    {
        case ".csv":
            result = new CsvSerialyzer(app);
            break;
        default:
            result = new XmlSerialyzer(app);
            break;
    }
    return result;
}
```

- **Generators** - пространство имен, для классов сериализаторов