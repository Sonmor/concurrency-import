﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public abstract class BaseSerialyzer : IDataGenerator
    {
        protected IEnumerable<Customer> customers;
        protected IGeneratorApp app;

        protected BaseSerialyzer(IGeneratorApp app)
        {
            this.app = app;
        }
        public void Generate(IEnumerable<Customer> customers)
        {
            this.customers = customers;
            using var stream = File.Create(app.OutputFile);
            SerializeData(stream);
            app.OnWrite($"Finished generated data in {app.OutputFile}...\n");
        }
        protected abstract void SerializeData(FileStream stream);
    }
}
