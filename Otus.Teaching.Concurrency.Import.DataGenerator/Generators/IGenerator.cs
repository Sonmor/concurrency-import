﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public interface IGenerator
    {
        List<Customer> Generate(int dataCount);
    }
}