using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.IO;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class XmlSerialyzer : BaseSerialyzer
    {
        public XmlSerialyzer(IGeneratorApp app) : base(app ){}
        
        protected override void SerializeData(FileStream stream)
        {
            app.OnWrite("Generating xml data...\n");
            new XmlSerializer(typeof(CustomersList)).Serialize(stream, new CustomersList()
            {
                Customers = customers
            });
        }
    }
}