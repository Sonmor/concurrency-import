﻿using CsvHelper;
using CsvHelper.Configuration;
using Otus.Teaching.Concurrency.Import.Core.IO;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Globalization;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvSerialyzer : BaseSerialyzer
    {
        private readonly CsvConfiguration config;
        public CsvSerialyzer(IGeneratorApp app) : base(app) 
        {
            config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                NewLine = Environment.NewLine
            };
        }
        protected override void SerializeData(FileStream stream)
        {
            app.OnWrite("Generating csv data...\n");
            using var serializer = new CsvWriter(new StreamWriter(stream), config);
            serializer.WriteRecords<Customer>(customers);
        }
    }
}
