using Otus.Teaching.Concurrency.Import.Core.IO;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataGenerator
{
    public class GeneratorFactory
    {
        private readonly IGeneratorApp app;
        public GeneratorFactory(IGeneratorApp app)
        {
            this.app = app;
        }
        public IDataGenerator GetGenerator()
        {
            IDataGenerator result;
            var ext = Path.GetExtension(app.OutputFile);
            switch (ext)
            {
                case ".csv":
                    result = new CsvSerialyzer(app);
                    break;
                default:
                    result = new XmlSerialyzer(app);
                    break;
            }
            return result;
        }
    }
}