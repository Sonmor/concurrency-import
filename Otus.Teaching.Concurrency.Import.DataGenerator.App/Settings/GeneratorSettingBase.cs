﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using System;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.App.Settings
{
    internal abstract class GeneratorSettingBase
    {
        public abstract Action<IGeneratorApp> Action { get; }
        public virtual void Run(IGeneratorApp application)
        {
            Action.Invoke(application);
        }
    }
}
