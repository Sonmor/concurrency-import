﻿using System;
using CommandLine;
using Otus.Teaching.Concurrency.Import.Core.IO;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.App.Settings
{
    [Verb("set", HelpText ="Set settings of generator ")]
    internal class GeneratorSettings : GeneratorSettingBase
    {
        [Option('o', "output", Required = true, HelpText ="Set output file for generated data: csv, xml and etc")]
        public string OutputFile { get; set; }

        [Option('c', "count", Required = false, Default = 100, HelpText = "Set number of Customers")]
        public int DataCount { get; set; }
        public override Action<IGeneratorApp> Action { get => (app) => 
        {
            app.OutputFile = OutputFile;
            app.DataCount = DataCount;
        }; }

    }
}
