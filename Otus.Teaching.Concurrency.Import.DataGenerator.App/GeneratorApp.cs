﻿using System;
using System.IO;
using Otus.Teaching.Concurrency.Import.Core.IO;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.App
{
    public class GeneratorApp:IGeneratorApp
    {
        private readonly IWriterUI Writer;
        private readonly IReaderUI Reader;
        private readonly IErrorUI Error;

        public event Action Exit;

        public string DefaultDirectory { get; set; } = AppDomain.CurrentDomain.BaseDirectory;
        public string OutputFile { get; set; } = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,"customers.xml");
        public int DataCount { get; set; } = 100;

        public GeneratorApp(IReaderUI reader, IWriterUI writer, IErrorUI error)
        {
            this.Reader = reader;
            this.Writer = writer;
            this.Error = error;
        }

        public string OnRead() => Reader.Read();
        public void OnWrite(string message) => Writer.Write(message);
        public void OnThrowError(string error) => Error.ThrowError(error);

        public void OnExit() => Exit.Invoke();
    }
}
