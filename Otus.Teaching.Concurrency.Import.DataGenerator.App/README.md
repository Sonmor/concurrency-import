# Otus.Teaching.Concurrency.Import.DataGenerator.App

Консольное приложение, которое позволяет при запуске отдельно выполнить генерацию файла из DataGenerator библиотеки.

## Структура проекта

- **GeneratorAppProgram** - класс содержит точку входа, и реализует прием и обработку аргументов командной строки, а так же основные этапы работы приложения:
    - Генерация данных
    - Парсинг данных
- **GeneratorApp** - класс хранящий методы вывода сообщений, вводв двнных от пользователя и вывода сообщений от ошибках.

- **Inf** - пространство имен содержит классы для взаимодействия с пользователем (вывод сообщений, ввод данных, вывод ошибок)

- **Settings** - пространство имен содержащие классы обработчики аргументов командной строки.

    Для обработки аргументов командной строки используется библотека ***[Command Line Parser](https://github.com/commandlineparser/commandline)***, которая позволяет удобно организовать обработку аргументов командной строки.

    **Вывод справки по всем ключам**

    ```bash
    > generator.exe --help
    ```

    ![generator.exe --help](img/generator_1.PNG)

    **Вывод информации о версии приложения**

     ```bash
     > generator.exe --version
     ```
    ![generator.exe --version](img/generator_2.PNG)

    **Вывод справки по опция конкретного ключа***
    ```bash
    > generator.exe set --help
    ```

    ![generator.exe set --help](img/generator_3.PNG)

    Удобный способ задания ключей командной строки

    ```c#
    [Verb("set", HelpText ="Set settings of generator ")]
    internal class GeneratorSettings : GeneratorSettingBase
    {
        [Option('o', "output", Required = true, HelpText ="Set output file for generated data: csv, xml and etc")]
        public string OutputFile { get; set; }

        [Option('c', "count", Required = false, Default = 100, HelpText = "Set number of Customers")]
        public int DataCount { get; set; }
        public override Action<IGeneratorApp> Action { get => (app) => 
        {
            app.OutputFile = OutputFile;
            app.DataCount = DataCount;
        }; } 

    ```

    Ключ **output** путь куда необходимо сохранить сгенерированный файл является обязательным. 

    Ключ **count** является не обязательным и его значение по умолчанию является 100

## Примеры использования

```bash
> generator.exe set -o .\customers.csv -c 1000
```