﻿using Otus.Teaching.Concurrency.Import.DataGenerator.App.Inf;
using Otus.Teaching.Concurrency.Import.DataGenerator.App.Settings;

using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using CommandLine;
using System.Reflection;
using CommandLine.Text;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Concurrency.Import.Core.IO;
using System;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.App
{
    internal class GeneratorAppProgram
    {

        static void Main(string[] args)
        {
            IGeneratorApp app = new GeneratorApp(new ConsoleReader(), new ConsoleWriter(), new ConsoleError());
            app.OnWrite("Generate data using generator.exe application\n");
            GeneratorAppProgram prog = new(app);
            prog.ParseSettings(args);

            GeneratorFactory factory = new GeneratorFactory(app);
            IDataGenerator generator = factory.GetGenerator();
            generator.Generate(new RandomCustomerGenerator().Generate(app.DataCount));
        }

        private readonly IGeneratorApp application;
        internal GeneratorAppProgram(IGeneratorApp app)
        {
            this.application = app;
            this.application.Exit += OnExit;
        }
        internal bool TryValidateAndParseArgs(string[] args)
        {
            if (args == null || args.Length == 0 ||args.Contains(string.Empty))
            {
                application.OnThrowError("Data file name without extension is required");
                return false;
            }

            ParseSettings(args);
            return true;
        }

        internal Parser parser { get; set; } = new Parser(with =>
        {
            with.HelpWriter = null;
            with.CaseSensitive = false;
            with.CaseInsensitiveEnumValues = true;
        });

        internal ParserResult<object> parserResult { get; set; }
        public void OnExit() => Environment.Exit(0); 

        public void ParseSettings(string[] args)
        {
            parserResult = parserResult ?? parser.ParseArguments(args, Assembly.GetExecutingAssembly().GetTypes().Where(t => t.BaseType == typeof(GeneratorSettingBase)).ToArray());

            parserResult.WithParsed(Run)
                .WithNotParsed(errs => CreateHelp(parserResult, errs));
        }
        private void Run(object obj) => (obj as GeneratorSettingBase).Run(application);
        private void CreateHelp<T>(ParserResult<T> result, IEnumerable<Error> errs)
        {
            HelpText helpText = errs.IsVersion()
                ? HelpText.AutoBuild(result)
                : HelpText.AutoBuild(result, h =>
                {
                    var name = Assembly.GetExecutingAssembly().GetName();
                    h.Heading = $"\n{name.Name}, Ver: {name.Version}";
                    h.Copyright = "Copyright (c) 2021";
                    h.AddPreOptionsLines(new string[]
                    {
                        "+--------------------------------------------------------+",
                        "|                     Customers generator                |",
                        "+--------------------------------------------------------+"
                    });
                    h.AddDashesToOption = true;
                    h.AddNewLineBetweenHelpSections = true;

                    return HelpText.DefaultParsingErrorsHandler(result, h);
                }, e => e, verbsIndex: true);

            application.OnWrite(helpText);
            application.OnExit();
        }
    }
}