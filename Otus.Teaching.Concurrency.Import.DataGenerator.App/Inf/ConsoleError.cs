﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using System;
namespace Otus.Teaching.Concurrency.Import.DataGenerator.App.Inf
{
    public class ConsoleError : IErrorUI
    {
        public void ThrowError(string error) 
            => Console.WriteLine($"Error: {error.Replace("\n", Environment.NewLine)}");
    }
}
