﻿namespace Otus.Teaching.Concurrency.Import.Core.IO
{
    public interface IReaderUI
    {
        string Read();
        void Clear();
    }
}
