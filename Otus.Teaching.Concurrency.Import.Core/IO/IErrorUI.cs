﻿namespace Otus.Teaching.Concurrency.Import.Core.IO
{
    public interface IErrorUI
    {
        void ThrowError(string error);
    }
}
