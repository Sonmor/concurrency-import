﻿namespace Otus.Teaching.Concurrency.Import.Core.IO
{
    public interface IWriterUI
    {
        void Write(string message);
    }
}
