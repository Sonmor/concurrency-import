﻿namespace Otus.Teaching.Concurrency.Import.Core.IO
{
    public enum LoaderType
    {
        SingleThread,
        ThreadPool
    }
}
