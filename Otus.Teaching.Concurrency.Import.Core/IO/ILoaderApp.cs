﻿namespace Otus.Teaching.Concurrency.Import.Core.IO
{
    public interface ILoaderApp:IGeneratorApp
    {
        string GeneratorPath { get; set; }
        LoaderType LoaderType { get; set; }
        int ThreadsCount { get; set; }
        void Configure();
    }
}