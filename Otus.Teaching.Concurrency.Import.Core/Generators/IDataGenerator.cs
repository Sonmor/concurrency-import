using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Handler.Data
{
    public interface IDataGenerator
    {
        void Generate(IEnumerable<Customer> customers);
    }
}