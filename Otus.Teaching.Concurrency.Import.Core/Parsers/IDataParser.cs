﻿namespace Otus.Teaching.Concurrency.Import.Core.Parsers
{
    public interface IDataParser<out T>
    {
        T Parse();
    }
}