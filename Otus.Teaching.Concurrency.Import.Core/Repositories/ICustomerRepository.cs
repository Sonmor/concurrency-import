using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {
        Task AddCustomersAsync(IEnumerable<Customer> customers);
        Task AddCustomerAsync(Customer customer);
        Task<Customer> FindCustomerAsync(int id);
        Task<List<Customer>> GetAllCustomersAsync();
    }
}