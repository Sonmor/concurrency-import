﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using System;
namespace Otus.Teaching.Concurrency.Import.Loader.Inf
{
    public class ConsoleWriter : IWriterUI
    {
        public void Write(string message)
            => Console.Write(message.Replace("\n", Environment.NewLine));
    }
}
