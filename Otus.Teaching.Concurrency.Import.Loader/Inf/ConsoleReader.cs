﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using System;
namespace Otus.Teaching.Concurrency.Import.Loader.Inf
{
    public class ConsoleReader : IReaderUI
    {
        public string Read() => Console.ReadLine();
    }
}
