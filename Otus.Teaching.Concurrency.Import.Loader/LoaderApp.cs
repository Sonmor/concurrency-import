﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using System;
using System.Configuration;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class LoaderApp : ILoaderApp
    {
        private readonly IWriterUI writer;
        private readonly IReaderUI reader;
        private readonly IErrorUI error;

        public event Action Exit;
        public string OutputFile { get; set; }
        public int DataCount { get; set; }
        public int ThreadsCount { get; set; }
        public string GeneratorPath { get; set; }
        public LoaderType LoaderType { get; set; }

        public LoaderApp(IWriterUI writer, IReaderUI reader, IErrorUI error)
        {
            this.writer = writer;
            this.reader = reader;
            this.error = error;
        }

        public void Configure()
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                if (appSettings.Count == 0)
                {
                    error.ThrowError("Settings is empty\n");
                }

                this.OutputFile = appSettings["OutputFile"];
                this.DataCount = int.Parse(appSettings["DataCount"]);
                this.LoaderType = Enum.Parse<LoaderType>(appSettings["Loader"]);
                this.ThreadsCount = int.Parse(appSettings["ThreadsCount"]);
            }
            catch (Exception ex)
            {
                error.ThrowError($"{ex.Message}\n");
                OnExit();
            }

        }
        public string OnRead() => reader.Read();
        public void OnThrowError(string error) => this.error.ThrowError(error);
        public void OnWrite(string message) => writer.Write(message);
        public void OnExit() => Exit.Invoke();
    }
}
