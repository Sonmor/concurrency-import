﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public abstract class LoaderBase:IDataLoader
    {
        protected readonly ILoaderApp app;
        protected readonly ICustomerRepository repository;

        protected LoaderBase(ILoaderApp app, ICustomerRepository repository)
        {
            this.app = app;
            this.repository = repository;
        }
        protected abstract void SetData(IEnumerable<Customer> customers);
        public void LoadData(IEnumerable<Customer> customers)
        {
            try
            {
                var watch = new Stopwatch();
                app.OnWrite("Start loading data .....\n");
                watch.Start();
                SetData(customers);
                watch.Stop();
                app.OnWrite("Finish loading data .....\n");
                app.OnWrite("\n ========= Report ==============\n");
                app.OnWrite($" Count recods: {app.DataCount}\n");
                app.OnWrite($" Loader Type: {Enum.GetName(app.LoaderType)}\n");
                app.OnWrite($" Thread Counts: {app.ThreadsCount}\n");
                app.OnWrite($" Loading spawn : {watch.Elapsed}\n");
                app.OnWrite(" ==================================\n");
            }
            catch(Exception ex)
            {
                app.OnThrowError(ex.Message);
                app.OnExit();
            }
        }
    }
}
