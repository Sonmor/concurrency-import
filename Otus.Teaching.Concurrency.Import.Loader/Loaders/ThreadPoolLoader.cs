﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    internal class ThreadPoolLoader:LoaderBase
    {
        public ThreadPoolLoader(ILoaderApp app, ICustomerRepository repo) : base(app, repo) { }
        protected override void SetData(IEnumerable<Customer> customers)
        {
            app.OnWrite("--------- Thread Pool Loader ------------\n");
            /// Разделение коллекции на части
            var chunks = customers.GroupBy(x => x.Id % app.ThreadsCount);
            var events = new List<ManualResetEvent>();

            foreach(var chunk in chunks)
            {
                var reset = new ManualResetEvent(false);
                
                ThreadPool.QueueUserWorkItem(arg => 
                { 
                    _ = repository.AddCustomersAsync(chunk);
                    reset.Set();
                });
                events.Add(reset);
            }
            WaitHandle.WaitAll(events.ToArray());
        }
    }
}