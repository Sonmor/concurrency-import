﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class SingleThreaderLoader : LoaderBase
    {
        public SingleThreaderLoader(ILoaderApp app, ICustomerRepository repo) : base(app, repo) { }

        protected override void SetData(IEnumerable<Customer> customers)
        {
            app.OnWrite("------   Single Thread Loader ---------\n");
           _ = repository.AddCustomersAsync(customers);
        }
    }
}