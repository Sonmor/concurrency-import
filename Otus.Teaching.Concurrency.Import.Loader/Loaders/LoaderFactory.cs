﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public static class LoaderFactory
    {
        public static IDataLoader GetLoader(ILoaderApp app, ICustomerRepository repo)
        {

            switch (app.LoaderType)
            {
                case LoaderType.SingleThread:
                    return new SingleThreaderLoader(app, repo);
                case LoaderType.ThreadPool:
                    return new ThreadPoolLoader(app, repo);
                default:
                    return new FakeDataLoader(app, repo);
            }
        }
    }
}
