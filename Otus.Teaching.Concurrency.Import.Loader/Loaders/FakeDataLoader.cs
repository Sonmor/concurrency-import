using Otus.Teaching.Concurrency.Import.Core.IO;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using System.Collections.Generic;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class FakeDataLoader : LoaderBase
    {
        public FakeDataLoader(ILoaderApp app, ICustomerRepository repo) : base(app, repo) { }
        protected override void SetData(IEnumerable<Customer> customers)
        {
            app.OnWrite(" -------- Fake Data Loader ------\n");
            Thread.Sleep(10000);
        }
    }
}