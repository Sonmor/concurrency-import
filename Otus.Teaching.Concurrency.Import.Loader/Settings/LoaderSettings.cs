﻿using System;
using CommandLine;
using Otus.Teaching.Concurrency.Import.Core.IO;

namespace Otus.Teaching.Concurrency.Import.Loader.Settings
{
    [Verb("set", HelpText ="Set settings of loader application ")]
    internal class LoaderSettings : LoaderSettingsBase
    {
        [Option('o', "output", Required = true, HelpText ="Set output file for generated data: csv, xml and etc")]
        public string OutputFile { get; set; }

        [Option('c', "count", Required = false, Default = 100, HelpText = "Set number of Customers")]
        public int DataCount { get; set; }

        [Option('g', "generator", HelpText ="Set path to generator")]
        public string GeneratorPath { get; set; }

        [Option('l', "loader", HelpText = "Set loader type")]
        public string LoaderType { get; set; }
        [Option('t', "threads", HelpText ="Set threads count")]
        public int ThreadsCount { get; set; }

        public override Action<ILoaderApp> Action { get => (app) => 
        {
            app.OutputFile = OutputFile;
            app.DataCount = DataCount;
            app.GeneratorPath = GeneratorPath;
            if(ThreadsCount >0 )app.ThreadsCount = ThreadsCount ;
            if (!string.IsNullOrEmpty(LoaderType))
            {
                
                if (!Enum.TryParse<LoaderType>(LoaderType, out LoaderType type))
                {
                    app.OnThrowError("Loader type is incorrect");
                    app.OnExit();
                }
                app.LoaderType = type;
            }
        }; }

    }
}
