﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using System;

namespace Otus.Teaching.Concurrency.Import.Loader.Settings
{
    internal abstract class LoaderSettingsBase
    {
        public abstract Action<ILoaderApp> Action { get; }
        public virtual void Run(ILoaderApp application)
        {
            Action.Invoke(application);
        }
    }
}
