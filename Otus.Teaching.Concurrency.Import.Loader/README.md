# Otus.Teaching.Concurrency.Import.Loader

 Консольное приложение, которое должно сгенирировать файл с данными и запустить загрузку из него через реализацию IDataLoader.

## Структура проекта

- **LoaderAppProgram** - класс содержит точку входа, и реализует прием и обработку аргументов командной строки, а так же основные этапы работы приложения:
    - Генерация данных
    - Парсинг данных
    - Загрузка данных в базу

- **LoaderApp** - класс хранящий методы вывода сообщений, вводв двнных от пользователя и вывода сообщений от ошибках.

- **LoaderModules** - наследник NinjectModules. Отпределяет определяет зависимости между классами DI контейнере.

- **Inf** - пространство имен содержит классы для взаимодействия с пользователем (вывод сообщений, ввод данных, вывод ошибок)

- **Loaders** - пространство имен содержит классы отвечающие за загрузку данных в БД

    - **LoaderFactory** - фабрика, создает класс-загрузчик в зависимости то заданных настроект или параметров коммандной строки.
    - **LoaderBase** - базовый для всех загрузчиков класс, определяет общий функционал

    ```c#
     public void LoadData(IEnumerable<Customer> customers)
        {
            try
            {
                var watch = new Stopwatch();
                app.OnWrite("Start loading data .....\n");
                watch.Start();
                SetData(customers);
                watch.Stop();
                app.OnWrite("Finish loading data .....\n");
                app.OnWrite("\n ========= Report ==============\n");
                app.OnWrite($" Count recods: {app.DataCount}\n");
                app.OnWrite($" Loader Type: {Enum.GetName(app.LoaderType)}\n");
                app.OnWrite($" Thread Counts: {app.ThreadsCount}\n");
                app.OnWrite($" Loading spawn : {watch.Elapsed}\n");
                app.OnWrite(" ==================================\n");
            }
            catch(Exception ex)
            {
                app.OnThrowError(ex.Message);
                app.OnExit();
            }
        }
    ```

    - **FakeDataLoader** - класс заглушка, ничего не делает, просто ждет.
    - **SingleThreaderLoader** - запускает загрузку данных в одном потоке.
    - **ThreadPoolLoader** - загрузка данных через ThreadPool

    ```c#
     protected override void SetData(IEnumerable<Customer> customers)
        {
            app.OnWrite("--------- Thread Pool Loader ------------\n");
            /// Разделение коллекции на части
            var chunks = customers.GroupBy(x => x.Id % app.ThreadsCount);
            var events = new List<ManualResetEvent>();

            foreach(var chunk in chunks)
            {
                var reset = new ManualResetEvent(false);
                
                ThreadPool.QueueUserWorkItem(arg => 
                { 
                    _ = repository.AddCustomersAsync(chunk);
                    reset.Set();
                });
                events.Add(reset);
            }
            WaitHandle.WaitAll(events.ToArray());
        }
        ```
- **Settings** - пространство имен содержащие классы обработчики аргументов командной строки.

    Для обработки аргументов командной строки используется библотека ***[Command Line Parser](https://github.com/commandlineparser/commandline)***, которая позволяет удобно организовать обработку аргументов командной строки.

    **Вывод справки по всем ключам**

    ```bash
    > loader.exe --help
    ```

    ![loader.exe --help](img/loader_1.PNG)

    **Вывод информации о версии приложения**

     ```bash
     > loader.exe --version
     ```
    ![loader.exe --version](img/loader_2.PNG)

    **Вывод справки по опция конкретного ключа***
    ```bash
    > loader.exe set --help
    ```

    ![loader.exe set --help](img/loader_3.PNG)

    Удобный способ задания ключей командной строки

    ```c#
     [Verb("set", HelpText ="Set settings of loader application ")]
    internal class LoaderSettings : LoaderSettingsBase
    {
        [Option('o', "output", Required = true, HelpText ="Set output file for generated data: csv, xml and etc")]
        public string OutputFile { get; set; }

        [Option('c', "count", Required = false, Default = 100, HelpText = "Set number of Customers")]
        public int DataCount { get; set; }

        [Option('g', "generator", HelpText ="Set path to generator")]
        public string GeneratorPath { get; set; }

        [Option('l', "loader", HelpText = "Set loader type")]
        public string LoaderType { get; set; }
        
        [Option('t', "threads", HelpText ="Set threads count")]
        public int ThreadsCount { get; set; }

        ......
    ```

    Ключ **output** путь куда необходимо сохранить сгенерированный файл является обязательным. Остальные ключи не обязательны и значения этих параметров беруться из файла конфигурации **App.config**. 

    Ключи командной строки имеею более высокий приоритет по сравнению с настройками по умолчанию в **App.config**

    Ключ **loader** задает тип загрузчика, имеет значение **SingleThread**, **ThreadPool**. Который создает соответствующий загрузчик или **FakeDataLoader** по умолчанию.

    Ключ **threads** задает кол-во потоков. В **App.config** задается 4 потока по умолчанию. Если тип загрузчика не **ThreadPool**, ключ игнорируется.

    Если ключ **generator** не задан или файла по указанному пути не существует, значит данные генерируются с использованием метода. Если путь задан и приложение по данному пити существует, то это приложение запускается в отдельном процессе и ему передаются ключи **output** и **count**

    ```c#
     internal void Generate()
        {
            if (app.GeneratorPath != null && !File.Exists(app.GeneratorPath))
            {
                app.OnThrowError("Generator file is not exist");
                app.OnExit();
            }
            if (app.GeneratorPath != null)
            {
                if (File.Exists(app.GeneratorPath))
                {
                    ProcessStartInfo procInfo = new ProcessStartInfo(app.GeneratorPath);
                    procInfo.Arguments = $@"set -o {app.OutputFile} -c {app.DataCount}";
                    var proc = Process.Start(procInfo);
                    app.OnWrite($"Generate {app.OutputFile} in process Id: {proc.Id}\n");
                }
            }
            else
            {
                app.OnWrite("Generate in method\n");

                GeneratorFactory factory = new GeneratorFactory(app);
                IDataGenerator generator = factory.GetGenerator();
                generator.Generate(new RandomCustomerGenerator().Generate(app.DataCount));
            }
        }
    ```

## Примеры использования

```bash
> loader.exe set  -o customer.csv -c 1000000 -l ThreadPool -t 8
```

```bash
> loader.exe set -g ..\..\..\..\bin\Debug\net5.0\generator.exe  -o customer.csv -c 100 -l SingleThread
```
