﻿using Ninject.Modules;
using Otus.Teaching.Concurrency.Import.Core.IO;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Inf;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class LoaderModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ILoaderApp>().To<LoaderApp>().InSingletonScope();
            Bind<IWriterUI>().To<ConsoleWriter>();
            Bind<IReaderUI>().To<ConsoleReader>();
            Bind<IErrorUI>().To<ConsoleError>();
            Bind<LoaderAppProgram>().ToSelf();
            Bind<ICustomerRepository>().To<CustomerRepository>();
        }
    }
}
