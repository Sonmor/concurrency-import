﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using CommandLine;
using CommandLine.Text;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Loader.Settings;
using Otus.Teaching.Concurrency.Import.Core.IO;
using Otus.Teaching.Concurrency.Import.Loader.Inf;
using System.IO;
using Otus.Teaching.Concurrency.Import.DataGenerator;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Ninject;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.DbContexts;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    internal class LoaderAppProgram
    {
        
        static void Main(string[] args)
        {
            var kernel = new StandardKernel(new LoaderModule());
            var repo = kernel.Get<ICustomerRepository>();
            using (CustomerDbContext context = new CustomerDbContext())
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var connection = context.Database.GetDbConnection();
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "PRAGMA journal_mode=WAL;";
                    command.ExecuteNonQuery();
                }
            }
                ILoaderApp application = kernel.Get<ILoaderApp>();
            LoaderAppProgram prog = new LoaderAppProgram(application);

            application.OnWrite($"Loader started with process Id {Process.GetCurrentProcess().Id}...\n");

            prog.ParseSettings(args);
            var loader = LoaderFactory.GetLoader(application, repo);

            prog.Generate();
            var parser = ParserFactory.GetParser(application);
            var customers = parser.Parse();
            loader.LoadData(customers);
            application.OnExit();
        }

        internal void Generate()
        {
            if (app.GeneratorPath != null && !File.Exists(app.GeneratorPath))
            {
                app.OnThrowError("Generator file is not exist");
                app.OnExit();
            }
            if (app.GeneratorPath != null)
            {
                if (File.Exists(app.GeneratorPath))
                {
                    ProcessStartInfo procInfo = new ProcessStartInfo(app.GeneratorPath);
                    procInfo.Arguments = $@"set -o {app.OutputFile} -c {app.DataCount}";
                    var proc = Process.Start(procInfo);
                    app.OnWrite($"Generate {app.OutputFile} in process Id: {proc.Id}\n");
                }
            }
            else
            {
                app.OnWrite("Generate in method\n");

                GeneratorFactory factory = new GeneratorFactory(app);
                IDataGenerator generator = factory.GetGenerator();
                generator.Generate(new RandomCustomerGenerator().Generate(app.DataCount));
            }
        }

        public void Exit()
        {
            _ = app.OnRead();
            Environment.Exit(0);
        }
        private readonly ILoaderApp app;
        internal LoaderAppProgram(ILoaderApp app)
        {
            this.app = app;
            app.Exit += Exit;
            app.Configure();
        }

        internal Parser parser { get; set; } = new Parser(with =>
        {
            with.HelpWriter = null;
            with.CaseSensitive = false;
            with.CaseInsensitiveEnumValues = true;
        });

        internal ParserResult<object> parserResult { get; set; }

        public void ParseSettings(string[] args)
        {
            parserResult = parserResult ?? parser.ParseArguments(args, Assembly.GetExecutingAssembly().GetTypes().Where(t => t.BaseType == typeof(LoaderSettingsBase)).ToArray());

            parserResult.WithParsed(Run)
                .WithNotParsed(errs => CreateHelp(parserResult, errs));
        }
        private void Run(object obj) => (obj as LoaderSettingsBase).Run(app);
        private void CreateHelp<T>(ParserResult<T> result, IEnumerable<Error> errs)
        {
            HelpText helpText = errs.IsVersion()
                ? HelpText.AutoBuild(result)
                : HelpText.AutoBuild(result, h =>
                {
                    var name = Assembly.GetExecutingAssembly().GetName();
                    h.Heading = $"\n{name.Name}, Ver: {name.Version}";
                    h.Copyright = "Copyright (c) 2021";
                    h.AddPreOptionsLines(new string[]
                    {
                        "+--------------------------------------------------------+",
                        "|               Loader customer information              |",
                        "+--------------------------------------------------------+"
                    });
                    h.AddDashesToOption = true;
                    h.AddNewLineBetweenHelpSections = true;

                    return HelpText.DefaultParsingErrorsHandler(result, h);
                }, e => e, verbsIndex: true);

            app.OnWrite(helpText);
            app.OnExit();
        }
    }
}